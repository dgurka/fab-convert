﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;

namespace FABConvert
{
    public partial class FrmMain : Form
    {
        int Journal_ID;
        private const int IMPORT_ADDRESS_WEIGHT = 10;
        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {

            txtConnString.Text="Provider=SQLOLEDB;Data Source=Adamtop\\SQLEXPRESS;Initial Catalog=FAB_WAT;User Id=fab;Password=fab;";
            //txtConnString.Text = "Provider=SQLOLEDB;Initial Catalog=FAB_WAT;Data Source=7TOP\\SQLEXPRESS;Initial Catalog=FAB_WAT;User Id=fab;Password=;";
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnBrowse_Click(this, null);
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog mydiag = new OpenFileDialog();
            mydiag.Filter = "Microsoft Access Database (*.mdb)|*.mdb";
            if (mydiag.ShowDialog() == DialogResult.OK)
            {
                txtOld.Text = mydiag.FileName;
            }
        }

        private void Error(string message)
        {
            lblStatus.Text = message;
            progStep.Value = 0;
            progConvert.Value = 0;
            txtLog.Text += message + "\r\n";
        }

        private void Status(string status)
        {
            lblStatus.Text = status;
            txtLog.Text += status + "...\r\n";
        }

        private Hashtable LoadFromDataReader(OleDbDataReader dr)
        {
            Hashtable retval = new Hashtable();
            for (int i = 0; i < dr.FieldCount; i++)
            {
                retval.Add(dr.GetName(i), dr.GetValue(i));
            }
            return retval;
        }

        private void DoEvents()
        {
            Application.DoEvents();
            if (btnConvert.Enabled == false)
                throw new Exception("Canceled!");
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            if (btnConvert.Text == "Cancel") {
                btnConvert.Enabled = false;
            }
            else if (btnConvert.Text == "Exit") {
                Application.Exit();
            }
            else {
                int i;
                Stream str = null;
                StreamReader strNewIDs = null;
                FileStream fstr = null;
                System.Reflection.Assembly a = null;
                OleDbConnection dbconnold = null;
                OleDbConnection dbconnnew = null;
                OleDbCommand cmdold = null;
                OleDbCommand cmdoldac = null;
                OleDbCommand cmdnew = null;
                OleDbCommand cmdnewreg = null;
                OleDbCommand cmdnewac = null;
                OleDbCommand cmdnewjournal = null;
                OleDbDataReader drold = null;
                OleDbDataReader droldac = null;
                btnConvert.Text = "Cancel";
                btnBrowseOld.Enabled = false;
                txtOld.Enabled = false;
                txtConnString.Enabled = false;
                btnClear.Enabled = false;

                txtLog.Text = "";

                int errors = 0;
                
                try {
                    progStep.Value = 0;
                    Status("Connecting to Databases");

                    dbconnold = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=\"" + txtOld.Text + "\";");

                    dbconnnew = new OleDbConnection(txtConnString.Text);
                    dbconnold.Open();
                    dbconnnew.Open();
                    if (dbconnold == null)
                        throw new Exception("Error connecting to old database");
                    if (dbconnnew == null)
                        throw new Exception("Error connecting to new database");
                    Application.DoEvents();

                    cmdold = new OleDbCommand();
                    cmdoldac = new OleDbCommand();
                    cmdnew = new OleDbCommand();
                    cmdnewreg = new OleDbCommand();
                    cmdnewac = new OleDbCommand();
                    cmdnewjournal = new OleDbCommand();
                    cmdold.Connection = dbconnold;
                    cmdoldac.Connection = dbconnold;
                    cmdnew.Connection = dbconnnew;
                    cmdnewreg.Connection = dbconnnew;
                    cmdnewac.Connection = dbconnnew;
                    cmdnewjournal.Connection = dbconnnew;
                    cmdold.CommandText = "SELECT count(*) as total FROM `Alarm Companies`";
                    drold = cmdold.ExecuteReader();
                    drold.Read();
                    int ac_count = (int)drold["total"];
                    drold.Close();
                    cmdold.CommandText = "SELECT count(*) as total FROM `Alarm Customers`";
                    drold = cmdold.ExecuteReader();
                    drold.Read();
                    int cust_count = (int)drold["total"];
                    int count = ac_count + cust_count;
                    drold.Close();


                    cmdnew.CommandText = "INSERT INTO FAB_Address(ID, Account_ID, Addressee, Street, City, State, Zip, False_Alarm_count, Billingl_Addressee, Billing_Street, Billing_City, Billing_State, Billing_Zip, Start_Date, End_Date, Exempt, StreetName, StreetNumber, Suite, Billing_Suite, PropertyType, ParcelNumber, Email, Phone, Cell, Billing_Email, Billing_Phone, Billing_Cell, FinanceID) SELECT TOP 1 (ID+1), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? FROM FAB_ADDRESS ORDER BY ID DESC";
                    setupFab_Address_Insert(cmdnew);

                    cmdnewreg.CommandText = "INSERT INTO FAB_Registration(account_id, contact_1, contact_2, permit_1_phone_24, permit_2_phone_24, permit_1_phone_home, permit_2_phone_home, permit_1_phone_work, permit_2_phone_work, permit_1_license, permit_2_license, permit_1_fax, permit_2_fax, permit_1_employeer, permit_2_employeer, permit_1_email, permit_2_email, responsible_1_name, responsible_2_name, responsible_1_day_phone, responsible_2_day_phone, responsible_1_night_phone, responsible_2_night_phone, responsible_1_cell_phone, responsible_2_cell_phone, billing_contact_phone, alarm_monitored, alarm_respond, alarm_subdivision, alarm_area, alarm_type, registration_initial, registration_expire, registration_last, permit_number, location_type, hazard_types, useful_info, hazard_info, license_state, license_county, license_city, notification_of_law, RegStatus, date_recorded, time_recorded, method_reported, permit_fee, fee_received, check_number, fee_other, administration_officer, Monitoring_Alarm_Company, Servicing_Alarm_Company) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    setupFab_Registration_Insert(cmdnewreg);

                    cmdnewac.CommandText = "INSERT INTO FAB_Alarm_Company(ID, Name, Addr1, Addr2, City, State, Zip, Suite, Contact, Phone, LicNumberFed, LicNumberState, LicNumberCounty, LicNumberCity, Monitoring, Servicing) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    setupFab_Alarm_Company_Insert(cmdnewac);

                    cmdnewjournal.CommandText = "INSERT INTO FAB_Journal(ID, Account_ID, Journal_Type_ID, Date_Time, Amount, Incident_ID, Notes, Entry_Date_Time, Disposition, IncidentType, BillDate, Paid) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    setupFab_Journal_Insert(cmdnewjournal);

                    cmdoldac.CommandText = "SELECT * FROM [Alarm Companies] ORDER BY AlarmCompanyName ASC";

                    cmdold.CommandText = "SELECT * FROM [Alarm Customers] ORDER BY [CompanyID] ASC";

                    progStep.Maximum = (int)count;
                    progConvert.Value = 0;
                    progConvert.Maximum = (int)count * IMPORT_ADDRESS_WEIGHT;

                    DoEvents();

                    Status("Loading Alarm Companies");

                    droldac = cmdoldac.ExecuteReader();

                    Hashtable[] alarm_companies = new Hashtable[ac_count];
                    Hashtable alarm_company_ID_translator = new Hashtable(ac_count);
                    i = 0;
                    while (droldac.Read()) {
                        alarm_companies[i] = LoadFromDataReader(droldac);
                        alarm_company_ID_translator.Add(alarm_companies[i]["AlarmCompanyCode"], i);
                        progStep.Value = i++;
                        progConvert.Increment(1);
                        DoEvents();
                    }

                    droldac.Close();

                    Status("Loading Alarm Customers");

                    drold = cmdold.ExecuteReader();

                    Hashtable[] alarm_customers = new Hashtable[cust_count];
                    i = 0;
                    while (drold.Read()) {
                        alarm_customers[i] = LoadFromDataReader(drold);
                        progStep.Value = ac_count + i++;
                        progConvert.Increment(1);
                        DoEvents();
                    }

                    drold.Close();

                    Status("Transferring Alarm Companies");

                    cmdold.Connection = dbconnnew;
                    cmdold.CommandText = "SELECT TOP 1 (ID+1) AS ID FROM FAB_Alarm_Company ORDER BY ID DESC";
                    drold = cmdold.ExecuteReader();
                    int AC_ID;
                    if (drold.Read())
                        AC_ID = (int)drold["ID"];
                    else
                        AC_ID = 1;
                    drold.Close();

                    cmdold.Connection = dbconnnew;
                    cmdold.CommandText = "SELECT TOP 1 (ID+1) AS ID FROM FAB_Journal ORDER BY ID DESC";
                    drold = cmdold.ExecuteReader();
                    if (drold.Read())
                        Journal_ID = (int)drold["ID"];
                    else
                        Journal_ID = 1;
                    drold.Close();

                    progStep.Value = 0;

                    for (i = 0; i < ac_count; i++) {
                        cmdnewac.Parameters["ID"].Value = AC_ID + i;
                        cmdnewac.Parameters["LicNumberCity"].Value = alarm_companies[i]["AlarmCompanyCode"];
                        cmdnewac.Parameters["Name"].Value = alarm_companies[i]["AlarmCompanyName"];
                        cmdnewac.Parameters["Phone"].Value = alarm_companies[i]["PhoneNumber"];
                        cmdnewac.Parameters["Addr1"].Value = alarm_companies[i]["Address1"];
                        cmdnewac.Parameters["City"].Value = alarm_companies[i]["Address2"];
                        cmdnewac.Parameters["State"].Value = alarm_companies[i]["Address3"];
                        cmdnewac.Parameters["Zip"].Value = alarm_companies[i]["ZipCode"];
                        cmdnewac.ExecuteNonQuery();
                        progStep.Value = i;
                        progConvert.Value = count + i*(IMPORT_ADDRESS_WEIGHT-1);
                        DoEvents();
                    }

                    Status("Transferring Alarm Customers");

                    cmdold.Connection = dbconnnew;
                    cmdold.CommandText = "SELECT TOP 1 (Account_ID+1) AS Account_ID FROM FAB_Address ORDER BY Account_ID DESC";
                    drold = cmdold.ExecuteReader();
                    int Account_ID;
                    if (drold.Read())
                    {
                        Account_ID = (int)drold["Account_ID"];
                        drold.Close();
                    }
                    else
                    {
                        Account_ID = 1;
                        drold.Close();
                        cmdold.CommandText = "INSERT INTO FAB_Address(ID, Account_ID) VALUES(0, 0)";
                        cmdold.ExecuteNonQuery();
                    }

                    cmdold.CommandText = "SELECT TOP 1 (Account_ID+1) AS Account_ID FROM FAB_Registration ORDER BY Account_ID DESC";
                    drold = cmdold.ExecuteReader();
                    int Account_ID2;
                    if (drold.Read())
                        Account_ID2 = (int)drold["Account_ID"];
                    else
                        Account_ID2 = 1;
                    drold.Close();

                    if (Account_ID < Account_ID2)
                        Account_ID = Account_ID2;

                    for (i = 0; i < cust_count; i++)
                    {
                        cmdnew.Parameters["Account_ID"].Value = i + Account_ID;
                        cmdnew.Parameters["Addressee"].Value = alarm_customers[i]["CompanyName"];
                        cmdnew.Parameters["StreetName"].Value = (Convert.ToString(alarm_customers[i]["StPrefix"]) + " " + Convert.ToString(alarm_customers[i]["StName"])).Trim();
                        cmdnew.Parameters["StreetNumber"].Value = alarm_customers[i]["StNumber"];
                        cmdnew.Parameters["Suite"].Value = alarm_customers[i]["Address2"];
                        cmdnew.Parameters["City"].Value = alarm_customers[i]["City"];
                        cmdnew.Parameters["State"].Value = alarm_customers[i]["State"];
                        cmdnew.Parameters["Zip"].Value = alarm_customers[i]["ZipCode"];
                        cmdnew.Parameters["Street"].Value = alarm_customers[i]["Street"];
                        cmdnew.Parameters["Phone"].Value = alarm_customers[i]["BusinessPhone"];
                        cmdnew.Parameters["Billing_Email"].Value = alarm_customers[i]["EmailAddress"];
                        cmdnew.Parameters["Billingl_Addressee"].Value = alarm_customers[i]["BName"];
                        cmdnew.Parameters["Billing_Street"].Value = alarm_customers[i]["BAddr1"];
                        if (alarm_customers[i]["BAddr2"].Equals(DBNull.Value))
                        {
                            cmdnew.Parameters["Billing_City"].Value = DBNull.Value;
                            cmdnew.Parameters["Billing_State"].Value = DBNull.Value;
                            cmdnew.Parameters["Billing_Zip"].Value = DBNull.Value;
                        }
                        else
                        {
                            string address_data = (string)alarm_customers[i]["BAddr2"];
                            string[] split = address_data.Split(' ');
                            if (split.Length == 1)
                            {
                                cmdnew.Parameters["Billing_City"].Value = split[0].Trim(',').Trim();
                                cmdnew.Parameters["Billing_State"].Value = DBNull.Value;
                                cmdnew.Parameters["Billing_Zip"].Value = DBNull.Value;
                            }
                            else if (split.Length == 2)
                            {
                                cmdnew.Parameters["Billing_City"].Value = split[0].Trim(',').Trim();
                                cmdnew.Parameters["Billing_State"].Value = split[1].Trim(',').Trim();
                                cmdnew.Parameters["Billing_Zip"].Value = DBNull.Value;
                            }
                            else
                            {
                                cmdnew.Parameters["Billing_City"].Value = split[0].Trim(',').Trim();
                                cmdnew.Parameters["Billing_State"].Value = split[1].Trim(',').Trim();
                                cmdnew.Parameters["Billing_Zip"].Value = String.Join(" ", split, 2, split.Length - 2).Trim(',').Trim();
                            }
                        }

                        if (!alarm_customers[i]["Notes"].Equals(DBNull.Value))
                        {
                            string note = (string)alarm_customers[i]["Notes"];
                            string[] split = note.Split(' ');
                            string date = split[0].Split(':')[0];
                            DateTime datetime;
                            if (DateTime.TryParse(date, out datetime))
                            {
                                note = String.Join(" ", split, 1, split.Length - 1).Trim();
                            } else {
                                datetime = DateTime.Now;
                            }
                            addNote(cmdnewjournal, i + Account_ID, note, datetime);
                        }
                        if (!alarm_customers[i]["NotifyDate"].Equals(DBNull.Value))
                            addNote(cmdnewjournal, i + Account_ID, "Notify Date 1", alarm_customers[i]["NotifyDate"]);
                        if (!alarm_customers[i]["NotifyDate2"].Equals(DBNull.Value))
                            addNote(cmdnewjournal, i + Account_ID, "Notify Date 2", alarm_customers[i]["NotifyDate2"]);
                        if (!alarm_customers[i]["RegLtr1Date"].Equals(DBNull.Value))
                            addNote(cmdnewjournal, i + Account_ID, "Registration Letter 1", alarm_customers[i]["RegLtr1Date"]);
                        if (!alarm_customers[i]["Ltr1RespDate"].Equals(DBNull.Value))
                            addNote(cmdnewjournal, i + Account_ID, "Registration Letter 1 Response", alarm_customers[i]["Ltr1RespDate"]);
                        if (!alarm_customers[i]["RegLtr2Date"].Equals(DBNull.Value))
                            addNote(cmdnewjournal, i + Account_ID, "Registration Letter 2", alarm_customers[i]["RegLtr2Date"]);
                        if (!alarm_customers[i]["RegLtr2Date"].Equals(DBNull.Value))
                            addNote(cmdnewjournal, i + Account_ID, "Registration Letter 2 Response", alarm_customers[i]["RegLtr2Date"]);
                        if (!alarm_customers[i]["RegLtr3Date"].Equals(DBNull.Value))
                            addNote(cmdnewjournal, i + Account_ID, "Registration Letter 3", alarm_customers[i]["RegLtr3Date"]);
                        if (!alarm_customers[i]["RegLtr3Date"].Equals(DBNull.Value))
                            addNote(cmdnewjournal, i + Account_ID, "Registration Letter 3 Response", alarm_customers[i]["RegLtr3Date"]);


                        cmdnewreg.Parameters["account_id"].Value = cmdnew.Parameters["Account_ID"].Value;
                        object ac_code = alarm_customers[i]["AlarmCompanyCode"];
                        if ((ac_code.Equals(DBNull.Value) || ac_code.Equals(null) || Convert.ToString(ac_code).Trim().Equals(""))) {
                            cmdnewreg.Parameters["Monitoring_Alarm_Company"].Value = DBNull.Value;
                        } else {
                            if (alarm_company_ID_translator.ContainsKey(ac_code)) {
                                cmdnewreg.Parameters["Monitoring_Alarm_Company"].Value = (object)((int)alarm_company_ID_translator[alarm_customers[i]["AlarmCompanyCode"]] + AC_ID);
                            }
                            else {
                                cmdnewreg.Parameters["Monitoring_Alarm_Company"].Value = DBNull.Value;
                            }
                        }


                        cmdnewreg.Parameters["Servicing_Alarm_Company"].Value = cmdnewreg.Parameters["Monitoring_Alarm_Company"].Value;
                        cmdnewreg.Parameters["registration_expire"].Value = (alarm_customers[i]["Registered"].Equals(DBNull.Value) || alarm_customers[i]["Registered"].Equals(false)) ? (object)DBNull.Value : new DateTime(2020,12,31);
                        cmdnewreg.Parameters["registration_initial"].Value = alarm_customers[i]["RegDate"];
                        if (cmdnewreg.Parameters["registration_initial"].Value.ToString() == "")
                        {
                            cmdnewreg.Parameters["registration_initial"].Value = System.DateTime.Parse("8/10/1961");
                            cmdnewreg.Parameters["RegStatus"].Value = "Renewal";
                        }
                        else
                        {
                            cmdnewreg.Parameters["RegStatus"].Value = DBNull.Value;
                        }
                        cmdnewreg.Parameters["useful_info"].Value = makeUsefull(alarm_customers[i]); // this is a stringBuilder-ish function

                        cmdnew.ExecuteNonQuery();
                        cmdnewreg.ExecuteNonQuery();

                        progStep.Value = ac_count + i;
                        progConvert.Value = count + (ac_count + i)*(IMPORT_ADDRESS_WEIGHT-1);
                        DoEvents();
                    }

                    cmdold.CommandText = "DELETE FROM FAB_Address WHERE ID=0";
                    cmdold.ExecuteNonQuery();

                    lblStatus.Text = "Imported " + count + " rows!";
                    txtLog.Text += lblStatus.Text;
                    btnConvert.Text = "Exit";
                }
                catch (Exception ex) {
                    Error(ex.Message);
                    btnConvert.Text = "Convert";
                    btnBrowseOld.Enabled = true;
                    txtOld.Enabled = true;
                    txtConnString.Enabled = true;
                }
                finally
                {
                    try
                    {
                        btnConvert.Enabled = true;
                        if (a != null)
                            a = null;
                        if (str != null)
                        {
                            str.Close();
                            str = null;
                        }
                        if (fstr != null)
                        {
                            fstr.Close();
                            fstr = null;
                        }
                        if (drold != null)
                        {
                            if (!drold.IsClosed)
                                drold.Close();
                            drold.Dispose();
                            drold = null;
                        }
                        if (droldac != null)
                        {
                            if (!droldac.IsClosed)
                                droldac.Close();
                            droldac.Dispose();
                            droldac = null;
                        }
                        if (cmdold != null)
                        {
                            cmdold.Dispose();
                            cmdold = null;
                        }
                        if (cmdoldac != null)
                        {
                            cmdoldac.Dispose();
                            cmdoldac = null;
                        }
                        if (cmdnew != null)
                        {
                            cmdnew.Dispose();
                            cmdnew = null;
                        }
                        if (dbconnold != null)
                        {
                            dbconnold.Close();
                            dbconnold.Dispose();
                            dbconnold = null;
                        }
                        if (dbconnnew != null)
                        {
                            dbconnnew.Close();
                            dbconnnew.Dispose();
                            dbconnnew = null;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error Garbage Collecting", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
        }

        private void addNote(OleDbCommand cmdnewjournal, int Account_ID, object Note, object datetime)
        {
            cmdnewjournal.Parameters["ID"].Value = Journal_ID++;
            cmdnewjournal.Parameters["Account_ID"].Value = Account_ID;
            cmdnewjournal.Parameters["Journal_Type_ID"].Value = 25;
            cmdnewjournal.Parameters["Date_Time"].Value = datetime;
            cmdnewjournal.Parameters["Amount"].Value = 0;
            cmdnewjournal.Parameters["Notes"].Value = Note;
            cmdnewjournal.Parameters["Entry_Date_Time"].Value = DateTime.Now;
            cmdnewjournal.ExecuteNonQuery();
        }

        private void setupFab_Address_Insert(OleDbCommand cmdnew)
        {
            cmdnew.Parameters.Add("Account_ID", OleDbType.BigInt);
            cmdnew.Parameters["Account_ID"].IsNullable = true;
            cmdnew.Parameters["Account_ID"].Value = DBNull.Value;
            cmdnew.Parameters.Add("Addressee", OleDbType.VarWChar, 100);
            cmdnew.Parameters["Addressee"].IsNullable = true;
            cmdnew.Parameters["Addressee"].Value = DBNull.Value;
            cmdnew.Parameters.Add("Street", OleDbType.VarWChar, 100);
            cmdnew.Parameters["Street"].IsNullable = true;
            cmdnew.Parameters["Street"].Value = DBNull.Value;
            cmdnew.Parameters.Add("City", OleDbType.VarWChar, 100);
            cmdnew.Parameters["City"].IsNullable = true;
            cmdnew.Parameters["City"].Value = DBNull.Value;
            cmdnew.Parameters.Add("State", OleDbType.VarWChar, 30);
            cmdnew.Parameters["State"].IsNullable = true;
            cmdnew.Parameters["State"].Value = DBNull.Value;
            cmdnew.Parameters.Add("Zip", OleDbType.VarWChar, 20);
            cmdnew.Parameters["Zip"].IsNullable = true;
            cmdnew.Parameters["Zip"].Value = DBNull.Value;
            cmdnew.Parameters.Add("False_Alarm_Count", OleDbType.Integer);
            cmdnew.Parameters["False_Alarm_Count"].IsNullable = false;
            cmdnew.Parameters["False_Alarm_Count"].Value = 0;
            cmdnew.Parameters.Add("Billingl_Addressee", OleDbType.VarWChar, 75);
            cmdnew.Parameters["Billingl_Addressee"].IsNullable = true;
            cmdnew.Parameters["Billingl_Addressee"].Value = DBNull.Value;
            cmdnew.Parameters.Add("Billing_Street", OleDbType.VarWChar, 50);
            cmdnew.Parameters["Billing_Street"].IsNullable = true;
            cmdnew.Parameters["Billing_Street"].Value = DBNull.Value;
            cmdnew.Parameters.Add("Billing_City", OleDbType.VarWChar, 50);
            cmdnew.Parameters["Billing_City"].IsNullable = true;
            cmdnew.Parameters["Billing_City"].Value = DBNull.Value;
            cmdnew.Parameters.Add("Billing_State", OleDbType.VarWChar, 50);
            cmdnew.Parameters["Billing_State"].IsNullable = true;
            cmdnew.Parameters["Billing_State"].Value = DBNull.Value;
            cmdnew.Parameters.Add("Billing_Zip", OleDbType.VarWChar, 50);
            cmdnew.Parameters["Billing_Zip"].IsNullable = true;
            cmdnew.Parameters["Billing_Zip"].Value = DBNull.Value;
            cmdnew.Parameters.Add("Start_Date", OleDbType.DBTimeStamp);
            cmdnew.Parameters["Start_Date"].IsNullable = true;
            cmdnew.Parameters["Start_Date"].Value = DBNull.Value;
            cmdnew.Parameters.Add("End_Date", OleDbType.DBTimeStamp);
            cmdnew.Parameters["End_Date"].IsNullable = true;
            cmdnew.Parameters["End_Date"].Value = DBNull.Value;
            cmdnew.Parameters.Add("Exempt", OleDbType.BigInt);
            cmdnew.Parameters["Exempt"].IsNullable = false;
            cmdnew.Parameters["Exempt"].Value = 0;
            cmdnew.Parameters.Add("StreetName", OleDbType.VarWChar, 50);
            cmdnew.Parameters["StreetName"].IsNullable = true;
            cmdnew.Parameters["StreetName"].Value = DBNull.Value;
            cmdnew.Parameters.Add("StreetNumber", OleDbType.VarWChar, 50);
            cmdnew.Parameters["StreetNumber"].IsNullable = true;
            cmdnew.Parameters["StreetNumber"].Value = DBNull.Value;
            cmdnew.Parameters.Add("Suite", OleDbType.VarWChar, 20);
            cmdnew.Parameters["Suite"].IsNullable = true;
            cmdnew.Parameters["Suite"].Value = DBNull.Value;
            cmdnew.Parameters.Add("Billing_Suite", OleDbType.VarWChar, 50);
            cmdnew.Parameters["Billing_Suite"].IsNullable = true;
            cmdnew.Parameters["Billing_Suite"].Value = DBNull.Value;
            cmdnew.Parameters.Add("PropertyType", OleDbType.TinyInt);
            cmdnew.Parameters["PropertyType"].IsNullable = true;
            cmdnew.Parameters["PropertyType"].Value = DBNull.Value;
            cmdnew.Parameters.Add("ParcelNumber", OleDbType.VarWChar, 25);
            cmdnew.Parameters["ParcelNumber"].IsNullable = true;
            cmdnew.Parameters["ParcelNumber"].Value = DBNull.Value;
            cmdnew.Parameters.Add("Email", OleDbType.VarWChar, 50);
            cmdnew.Parameters["Email"].IsNullable = true;
            cmdnew.Parameters["Email"].Value = DBNull.Value;
            cmdnew.Parameters.Add("Phone", OleDbType.VarWChar, 15);
            cmdnew.Parameters["Phone"].IsNullable = true;
            cmdnew.Parameters["Phone"].Value = DBNull.Value;
            cmdnew.Parameters.Add("Cell", OleDbType.VarWChar, 15);
            cmdnew.Parameters["Cell"].IsNullable = true;
            cmdnew.Parameters["Cell"].Value = DBNull.Value;
            cmdnew.Parameters.Add("Billing_Email", OleDbType.VarWChar, 50);
            cmdnew.Parameters["Billing_Email"].IsNullable = true;
            cmdnew.Parameters["Billing_Email"].Value = DBNull.Value;
            cmdnew.Parameters.Add("Billing_Phone", OleDbType.VarWChar, 15);
            cmdnew.Parameters["Billing_Phone"].IsNullable = true;
            cmdnew.Parameters["Billing_Phone"].Value = DBNull.Value;
            cmdnew.Parameters.Add("Billing_Cell", OleDbType.VarWChar, 15);
            cmdnew.Parameters["Billing_Cell"].IsNullable = true;
            cmdnew.Parameters["Billing_Cell"].Value = DBNull.Value;
            cmdnew.Parameters.Add("FinanceID", OleDbType.BigInt);
            cmdnew.Parameters["FinanceID"].IsNullable = true;
            cmdnew.Parameters["FinanceID"].Value = DBNull.Value;
        }
        private void setupFab_Registration_Insert(OleDbCommand cmdnewreg)
        {
            cmdnewreg.Parameters.Add("account_id", OleDbType.BigInt);
            cmdnewreg.Parameters["account_id"].IsNullable = true;
            cmdnewreg.Parameters["account_id"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("contact_1", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["contact_1"].IsNullable = true;
            cmdnewreg.Parameters["contact_1"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("contact_2", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["contact_2"].IsNullable = true;
            cmdnewreg.Parameters["contact_2"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("permit_1_phone_24", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["permit_1_phone_24"].IsNullable = true;
            cmdnewreg.Parameters["permit_1_phone_24"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("permit_2_phone_24", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["permit_2_phone_24"].IsNullable = true;
            cmdnewreg.Parameters["permit_2_phone_24"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("permit_1_phone_home", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["permit_1_phone_home"].IsNullable = true;
            cmdnewreg.Parameters["permit_1_phone_home"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("permit_2_phone_home", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["permit_2_phone_home"].IsNullable = true;
            cmdnewreg.Parameters["permit_2_phone_home"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("permit_1_phone_work", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["permit_1_phone_work"].IsNullable = true;
            cmdnewreg.Parameters["permit_1_phone_work"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("permit_2_phone_work", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["permit_2_phone_work"].IsNullable = true;
            cmdnewreg.Parameters["permit_2_phone_work"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("permit_1_license", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["permit_1_license"].IsNullable = true;
            cmdnewreg.Parameters["permit_1_license"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("permit_2_license", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["permit_2_license"].IsNullable = true;
            cmdnewreg.Parameters["permit_2_license"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("permit_1_fax", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["permit_1_fax"].IsNullable = true;
            cmdnewreg.Parameters["permit_1_fax"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("permit_2_fax", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["permit_2_fax"].IsNullable = true;
            cmdnewreg.Parameters["permit_2_fax"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("permit_1_employeer", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["permit_1_employeer"].IsNullable = true;
            cmdnewreg.Parameters["permit_1_employeer"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("permit_2_employeer", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["permit_2_employeer"].IsNullable = true;
            cmdnewreg.Parameters["permit_2_employeer"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("permit_1_email", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["permit_1_email"].IsNullable = true;
            cmdnewreg.Parameters["permit_1_email"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("permit_2_email", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["permit_2_email"].IsNullable = true;
            cmdnewreg.Parameters["permit_2_email"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("responsible_1_name", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["responsible_1_name"].IsNullable = true;
            cmdnewreg.Parameters["responsible_1_name"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("responsible_2_name", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["responsible_2_name"].IsNullable = true;
            cmdnewreg.Parameters["responsible_2_name"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("responsible_1_day_phone", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["responsible_1_day_phone"].IsNullable = true;
            cmdnewreg.Parameters["responsible_1_day_phone"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("responsible_2_day_phone", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["responsible_2_day_phone"].IsNullable = true;
            cmdnewreg.Parameters["responsible_2_day_phone"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("responsible_1_night_phone", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["responsible_1_night_phone"].IsNullable = true;
            cmdnewreg.Parameters["responsible_1_night_phone"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("responsible_2_night_phone", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["responsible_2_night_phone"].IsNullable = true;
            cmdnewreg.Parameters["responsible_2_night_phone"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("responsible_1_cell_phone", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["responsible_1_cell_phone"].IsNullable = true;
            cmdnewreg.Parameters["responsible_1_cell_phone"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("responsible_2_cell_phone", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["responsible_2_cell_phone"].IsNullable = true;
            cmdnewreg.Parameters["responsible_2_cell_phone"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("billing_contact_phone", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["billing_contact_phone"].IsNullable = true;
            cmdnewreg.Parameters["billing_contact_phone"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("alarm_monitored", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["alarm_monitored"].IsNullable = true;
            cmdnewreg.Parameters["alarm_monitored"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("alarm_respond", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["alarm_respond"].IsNullable = true;
            cmdnewreg.Parameters["alarm_respond"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("alarm_subdivision", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["alarm_subdivision"].IsNullable = true;
            cmdnewreg.Parameters["alarm_subdivision"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("alarm_area", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["alarm_area"].IsNullable = true;
            cmdnewreg.Parameters["alarm_area"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("alarm_type", OleDbType.BigInt);
            cmdnewreg.Parameters["alarm_type"].IsNullable = true;
            cmdnewreg.Parameters["alarm_type"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("registration_initial", OleDbType.DBTimeStamp);
            cmdnewreg.Parameters["registration_initial"].IsNullable = true;
            cmdnewreg.Parameters["registration_initial"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("registration_expire", OleDbType.DBTimeStamp);
            cmdnewreg.Parameters["registration_expire"].IsNullable = true;
            cmdnewreg.Parameters["registration_expire"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("registration_last", OleDbType.DBTimeStamp);
            cmdnewreg.Parameters["registration_last"].IsNullable = true;
            cmdnewreg.Parameters["registration_last"].Value = System.DateTime.Today;
            cmdnewreg.Parameters.Add("permit_number", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["permit_number"].IsNullable = true;
            cmdnewreg.Parameters["permit_number"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("location_type", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["location_type"].IsNullable = true;
            cmdnewreg.Parameters["location_type"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("hazard_types", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["hazard_types"].IsNullable = true;
            cmdnewreg.Parameters["hazard_types"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("useful_info", OleDbType.VarWChar);
            cmdnewreg.Parameters["useful_info"].IsNullable = true;
            cmdnewreg.Parameters["useful_info"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("hazard_info", OleDbType.VarWChar);
            cmdnewreg.Parameters["hazard_info"].IsNullable = true;
            cmdnewreg.Parameters["hazard_info"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("license_state", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["license_state"].IsNullable = true;
            cmdnewreg.Parameters["license_state"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("license_county", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["license_county"].IsNullable = true;
            cmdnewreg.Parameters["license_county"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("license_city", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["license_city"].IsNullable = true;
            cmdnewreg.Parameters["license_city"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("notification_of_law", OleDbType.DBTimeStamp);
            cmdnewreg.Parameters["notification_of_law"].IsNullable = true;
            cmdnewreg.Parameters["notification_of_law"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("RegStatus", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["RegStatus"].IsNullable = true;
            cmdnewreg.Parameters["RegStatus"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("date_recorded", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["date_recorded"].IsNullable = true;
            cmdnewreg.Parameters["date_recorded"].Value = System.DateTime.Today.Date;
            cmdnewreg.Parameters.Add("time_recorded", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["time_recorded"].IsNullable = true;
            cmdnewreg.Parameters["time_recorded"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("method_reported", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["method_reported"].IsNullable = true;
            cmdnewreg.Parameters["method_reported"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("permit_fee", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["permit_fee"].IsNullable = true;
            cmdnewreg.Parameters["permit_fee"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("fee_received", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["fee_received"].IsNullable = true;
            cmdnewreg.Parameters["fee_received"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("check_number", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["check_number"].IsNullable = true;
            cmdnewreg.Parameters["check_number"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("fee_other", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["fee_other"].IsNullable = true;
            cmdnewreg.Parameters["fee_other"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("administration_officer", OleDbType.VarWChar, 50);
            cmdnewreg.Parameters["administration_officer"].IsNullable = true;
            cmdnewreg.Parameters["administration_officer"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("Monitoring_Alarm_Company", OleDbType.BigInt);
            cmdnewreg.Parameters["Monitoring_Alarm_Company"].IsNullable = true;
            cmdnewreg.Parameters["Monitoring_Alarm_Company"].Value = DBNull.Value;
            cmdnewreg.Parameters.Add("Servicing_Alarm_Company", OleDbType.BigInt);
            cmdnewreg.Parameters["Servicing_Alarm_Company"].IsNullable = true;
            cmdnewreg.Parameters["Servicing_Alarm_Company"].Value = DBNull.Value;
        }
        private void setupFab_Alarm_Company_Insert(OleDbCommand cmdnewac)
        {
            cmdnewac.Parameters.Add("ID", OleDbType.Integer);
            cmdnewac.Parameters.Add("Name", OleDbType.VarWChar, 50);
            cmdnewac.Parameters["Name"].IsNullable = true;
            cmdnewac.Parameters["Name"].Value = DBNull.Value;
            cmdnewac.Parameters.Add("Addr1", OleDbType.VarWChar, 50);
            cmdnewac.Parameters["Addr1"].IsNullable = true;
            cmdnewac.Parameters["Addr1"].Value = DBNull.Value;
            cmdnewac.Parameters.Add("Addr2", OleDbType.VarWChar, 150);
            cmdnewac.Parameters["Addr2"].IsNullable = true;
            cmdnewac.Parameters["Addr2"].Value = DBNull.Value;
            cmdnewac.Parameters.Add("City", OleDbType.VarWChar, 50);
            cmdnewac.Parameters["City"].IsNullable = true;
            cmdnewac.Parameters["City"].Value = DBNull.Value;
            cmdnewac.Parameters.Add("State", OleDbType.VarWChar, 50);
            cmdnewac.Parameters["State"].IsNullable = true;
            cmdnewac.Parameters["State"].Value = DBNull.Value;
            cmdnewac.Parameters.Add("Zip", OleDbType.VarWChar, 15);
            cmdnewac.Parameters["Zip"].IsNullable = true;
            cmdnewac.Parameters["Zip"].Value = DBNull.Value;
            cmdnewac.Parameters.Add("Suite", OleDbType.VarWChar, 50);
            cmdnewac.Parameters["Suite"].IsNullable = true;
            cmdnewac.Parameters["Suite"].Value = DBNull.Value;
            cmdnewac.Parameters.Add("Contact", OleDbType.VarWChar, 50);
            cmdnewac.Parameters["Contact"].IsNullable = true;
            cmdnewac.Parameters["Contact"].Value = DBNull.Value;
            cmdnewac.Parameters.Add("Phone", OleDbType.VarWChar, 25);
            cmdnewac.Parameters["Phone"].IsNullable = true;
            cmdnewac.Parameters["Phone"].Value = DBNull.Value;
            cmdnewac.Parameters.Add("LicNumberFed", OleDbType.VarWChar, 20);
            cmdnewac.Parameters["LicNumberFed"].IsNullable = true;
            cmdnewac.Parameters["LicNumberFed"].Value = DBNull.Value;
            cmdnewac.Parameters.Add("LicNumberState", OleDbType.VarWChar, 20);
            cmdnewac.Parameters["LicNumberState"].IsNullable = true;
            cmdnewac.Parameters["LicNumberState"].Value = DBNull.Value;
            cmdnewac.Parameters.Add("LicNumberCounty", OleDbType.VarWChar, 20);
            cmdnewac.Parameters["LicNumberCounty"].IsNullable = true;
            cmdnewac.Parameters["LicNumberCounty"].Value = DBNull.Value;
            cmdnewac.Parameters.Add("LicNumberCity", OleDbType.VarWChar, 20);
            cmdnewac.Parameters["LicNumberCity"].IsNullable = true;
            cmdnewac.Parameters["LicNumberCity"].Value = DBNull.Value;
            cmdnewac.Parameters.Add("Monitoring", OleDbType.Integer);
            cmdnewac.Parameters["Monitoring"].IsNullable = true;
            cmdnewac.Parameters["Monitoring"].Value = DBNull.Value;
            cmdnewac.Parameters.Add("Servicing", OleDbType.Integer);
            cmdnewac.Parameters["Servicing"].IsNullable = true;
            cmdnewac.Parameters["Servicing"].Value = DBNull.Value;
        }
        private void setupFab_Journal_Insert(OleDbCommand cmdnewjournal)
        {
            cmdnewjournal.Parameters.Add("ID", OleDbType.Integer);
            cmdnewjournal.Parameters.Add("Account_ID", OleDbType.Integer);
            cmdnewjournal.Parameters.Add("Journal_Type_ID", OleDbType.SmallInt);
            cmdnewjournal.Parameters.Add("Date_Time", OleDbType.DBTimeStamp);
            cmdnewjournal.Parameters.Add("Amount", OleDbType.Currency);
            cmdnewjournal.Parameters.Add("Incident_ID", OleDbType.VarWChar, 50);
            cmdnewjournal.Parameters["Incident_ID"].IsNullable = true;
            cmdnewjournal.Parameters["Incident_ID"].Value = DBNull.Value;
            cmdnewjournal.Parameters.Add("Notes", OleDbType.WChar);
            cmdnewjournal.Parameters["Notes"].IsNullable = true;
            cmdnewjournal.Parameters["Notes"].Value = DBNull.Value;
            cmdnewjournal.Parameters.Add("Entry_Date_Time", OleDbType.DBTimeStamp, 50);
            cmdnewjournal.Parameters["Entry_Date_Time"].IsNullable = true;
            cmdnewjournal.Parameters["Entry_Date_Time"].Value = DBNull.Value;
            cmdnewjournal.Parameters.Add("Disposition", OleDbType.Integer);
            cmdnewjournal.Parameters["Disposition"].IsNullable = true;
            cmdnewjournal.Parameters["Disposition"].Value = DBNull.Value;
            cmdnewjournal.Parameters.Add("IncidentType", OleDbType.VarWChar, 15);
            cmdnewjournal.Parameters["IncidentType"].IsNullable = true;
            cmdnewjournal.Parameters["IncidentType"].Value = DBNull.Value;
            cmdnewjournal.Parameters.Add("BillDate", OleDbType.DBTimeStamp);
            cmdnewjournal.Parameters["BillDate"].IsNullable = true;
            cmdnewjournal.Parameters["BillDate"].Value = DBNull.Value;
            cmdnewjournal.Parameters.Add("Paid", OleDbType.TinyInt);
            cmdnewjournal.Parameters["Paid"].IsNullable = true;
            cmdnewjournal.Parameters["Paid"].Value = DBNull.Value;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to clear the target database?", this.Text, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.OK)
            {
                try
                {
                    txtLog.Text = "";
                    Status("Opening Connection to new Database");
                    OleDbConnection conn = new OleDbConnection(txtConnString.Text);
                    conn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.Connection = conn;
                    Application.DoEvents();

                    Status("Deleting Old Alarm Companies");
                    cmd.CommandText = "DELETE FROM FAB_Alarm_Company";
                    cmd.ExecuteNonQuery();
                    Application.DoEvents();

                    Status("Deleting Old Addresses");
                    cmd.CommandText = "DELETE FROM FAB_Address";
                    cmd.ExecuteNonQuery();
                    Application.DoEvents();

                    Status("Deleting Old Registrations");
                    cmd.CommandText = "DELETE FROM FAB_Registration";
                    cmd.ExecuteNonQuery();
                    Application.DoEvents();

                    Status("Deleting Old Journal Entries");
                    cmd.CommandText = "DELETE FROM FAB_Journal";
                    cmd.ExecuteNonQuery();
                    Application.DoEvents();

                    lblStatus.Text = "Old records cleared!";
                    txtLog.Text += lblStatus.Text;
                    Application.DoEvents();
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                }
                
            }
        }
    
        /*
         * I am doing this the old school way to be faster
         */
        private string makeUsefull(Hashtable row)
        {
            string data = "<Root Customer = 'Waterford'>";
            data += "<KeyHolders Category='Contact'>";

            for (int i = 1; i <= 6; i++)
            {
                //string a = row["CName" + i].ToString();
                if (row["CName" + i].ToString() != "")
                {
                    // write out a keyholder
                    data += "<Entry ID='" + i + "'>";
                    data += "<name>" + row["CName" + i] + "</name>";
                    data += "<phone_24_hour>" + row["CPhone" + i] + "</phone_24_hour>";
                    data += "<home_phone/><work_phone/><fax/><email/><Drivers_License/><employer_name/></Entry>";
                }
            }

            data += "</KeyHolders><Responsible Category=`Contact`><Entry ID=`1`>";
            data += "<name>" + row["BAddr4"] + "</name><day_phone/><night_phone/><cell_phone/></Entry><Entry ID=`2`>";
            data += "<name>" + row["BldgOwner"] + "</name>";
            data += "<day_phone>" + row["BldgOwnerPhone1"] + "</day_phone><night_phone/>";
            data += "<night_phone>" + row["BldgOwnerPhone2"] + "</night_phone></Entry></Responsible><Hazards/>";
            data += "<LocationInfo/><AlarmSystems><Entry ID=`1`><type>monitored</type></Entry></AlarmSystems></Root>";

            return data;
        }
    }
}
